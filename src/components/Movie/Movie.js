import React, {Component} from 'react';
import './Movie.css';

class Movie extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.title !== this.props.title;
    }

    render() {
        return (
            <div className="Movie">
                <input type="text" value={this.props.title} onChange={this.props.edit}/>
                <span onClick={this.props.remove}>remove</span>
            </div>
        )
    }
}

export default Movie;