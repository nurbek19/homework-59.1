import React, {Component} from 'react';
import Movie from '../../components/Movie/Movie';
import './MovieList.css';

class MovieList extends Component {
    state = {
        movies: [],
        currentMovie: ''
    };

    addTitle = (event) => {
        this.setState({currentMovie: event.target.value});
    };

    addMovie = (e) => {
        e.preventDefault();
        let currentMovie = this.state.currentMovie;

        if (currentMovie !== '') {
            const movies = [...this.state.movies];
            const date = new Date();
            const time = date.getTime();
            const movie = {
                title: currentMovie,
                id: time
            };

            movies.unshift(movie);

            this.setState({movies, currentMovie: ''});

        } else {
            alert('Please add movie!');
        }
    };

    editMovie = (event, id) => {
        const index = this.state.movies.findIndex(m => m.id === id);
        const movie = {...this.state.movies[index]};
        movie.title = event.target.value;

        const movies = [...this.state.movies];
        movies[index] = movie;

        this.setState({movies});
    };

    removeMovie = (id) => {
        const index = this.state.movies.findIndex(m => m.id === id);

        const movies = [...this.state.movies];
        movies.splice(index, 1);

        this.setState({movies});
    };

    render() {

        const moviesList = this.state.movies.map((movie) => {
            return <Movie
                title={movie.title}
                key={movie.id}
                edit={(event) => this.editMovie(event, movie.id)}
                remove={() => this.removeMovie(movie.id)}
            />
        });

        return (
            <div className="MovieList">
                <form action="#">
                    <input
                        type="text"
                        placeholder="Add movie"
                        onChange={this.addTitle}
                        value={this.state.currentMovie}
                    />
                    <button onClick={this.addMovie}>Add</button>
                </form>

                <div className="Movies">
                    <h4>To watch list:</h4>
                    {moviesList}
                </div>
            </div>
        )
    }
}

export default MovieList;