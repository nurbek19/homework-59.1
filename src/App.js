import React, { Component } from 'react';
import MovieList from './containers/MovieList/MovieList';

class App extends Component {
  render() {
    return (
      <MovieList/>
    );
  }
}

export default App;
